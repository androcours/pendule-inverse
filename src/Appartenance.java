
public class Appartenance {
    String nom;
    float etat;
    float max,min;
    int id;

    public Appartenance(String nom, float etat, float max, float min, int id) {
        this.nom = nom;
        this.etat = etat;
        this.max = max;
        this.min = min;
        this.id = id;
    }
    
    public static Appartenance getAppartenance(Regle regle){
        return new Appartenance(regle.nom, regle.etat, regle.max, regle.min, 0);
    }
}
