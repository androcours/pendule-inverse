import java.util.Scanner;

public class PenduleInverse {

    public PenduleInverse() {
        Scanner in = new Scanner(System.in);

        System.out.println("Entrez un angle :");

        int angle = in.nextInt();

        System.out.println("Entrez une vitesse :");

        float vitesse = in.nextFloat();

        Regle regleResultat[] = new Regle[4];
        Regle regles[][] = new Regle[5][5];
        Regle regleAngle[] = new Regle[2];
        Regle regleVitesse[] = new Regle[2];

        regleAngle[0]= new Regle(null, 0, 0, 0, 0, 0, 0, null, null);
        regleAngle[1]= new Regle(null, 0, 0, 0, 0, 0, 0, null, null);
        regleVitesse[0]= new Regle(null, 0, 0, 0, 0, 0, 0, null, null);
        regleVitesse[1]= new Regle(null, 0, 0, 0, 0, 0, 0, null, null);

        int nbRegleResultat = 0;

        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                regles[i][j] = new Regle("OO",0,0,0,0,0,0,null, null);
            }
        }

        regles[0][2] = new Regle("NM",0,0,-45,-90,0,0,null, null);
        regles[1][1] = new Regle("NS",0,0,0,-90,0,0,null, null);
        regles[1][3] = new Regle("ZR",0,0,45,-45,0,0,null, null);
        regles[2][2] = new Regle("ZR",0,0,45,-45,0,0,null, null);
        regles[3][1] = new Regle("ZR",0,0,45,-45,0,0,null, null);
        regles[3][3] = new Regle("PS",0,0,90,0,0,0,null, null);
        regles[4][2] = new Regle("PM",0,0,90,45,0,0,null, null);

        System.out.println("-----Fuzzification-----");

        int nbRegleAngle = Evaluation(angle, regleAngle);
        float nbRegleVitesse = Evaluation(vitesse, regleVitesse);


        for (int i = 0; i < nbRegleAngle; i++) {
            System.out.println("Appartenance Angle "+i+" : "+regleAngle[i].nom+" "+regleAngle[i].etat);
        }

        for (int i = 0; i < nbRegleVitesse; i++) {
            System.out.println("Appartenance Vitesse "+i+" : "+regleVitesse[i].nom+" "+regleVitesse[i].etat);
        }

        System.out.println("-----Application des règles-----");

        for (int i = 0; i < nbRegleAngle; i++) {
            for (int j = 0; j < nbRegleVitesse; j++) {
                int x = getIndexFromName(regleAngle[i].nom);
                int y = getIndexFromName(regleVitesse[j].nom);


                if (x == -1 || y == -1) continue;

                if(regles[x][y].nom.equals("OO")) continue;

                regleResultat[nbRegleResultat] = regles[x][y];
                regleResultat[nbRegleResultat].Appartenance1 = Appartenance.getAppartenance(regleAngle[i]);
                regleResultat[nbRegleResultat].Appartenance2 = Appartenance.getAppartenance(regleVitesse[j]);

                nbRegleResultat++;

            }
        }

        float intervalmax  = -1000;
        float intervalMin  = 1000;

        for(int i = 0; i != nbRegleResultat;i++){

            System.out.println("Regle " + i +" : "+ regleResultat[i].nom);
            System.out.println("Appartenance 1 : "+ regleResultat[i].Appartenance1.nom+ " : "+ regleResultat[i].Appartenance1.etat);
            System.out.println("Appartenance 2 : "+ regleResultat[i].Appartenance2.nom+ " : "+ regleResultat[i].Appartenance2.etat);



            if( regleResultat[i].Appartenance1.etat < regleResultat[i].Appartenance2.etat){

                if(  regleResultat[i].Appartenance1.max > intervalmax )
                    intervalmax = regleResultat[i].Appartenance1.max;

                if(  regleResultat[i].Appartenance1.min < intervalMin )
                    intervalMin =  regleResultat[i].Appartenance1.min;
            }
            else {

                if(  regleResultat[i].Appartenance2.max > intervalmax )
                    intervalmax =  regleResultat[i].Appartenance2.max;

                if(  regleResultat[i].Appartenance2.min < intervalMin )
                    intervalMin = regleResultat[i].Appartenance2.min;

                regleResultat[i].Appartenance1 = regleResultat[i].Appartenance2;

            }
        }


        System.out.println("Regle actif : "+ nbRegleResultat);
        for(int IndexRegle = 0; IndexRegle != nbRegleResultat;IndexRegle++){
            System.out.println("Regle "+IndexRegle+"  Appartenance\tMin : "+ regleResultat[IndexRegle].Appartenance1.etat +" \t Nom : "+ regleResultat[IndexRegle].Appartenance1.nom);
        }


        float valTest;
        float value = 0;
        float numerateur= 0;
        float denominateur = 0;

        System.out.println("-----Défuzzification-----");


        for ( float i = intervalMin ; i <  intervalmax ; i++){
            value = -1;

            for(int IndexRegle = 0; IndexRegle != nbRegleResultat;IndexRegle++){

                valTest =  FonctionAppartenance(i,regleResultat[IndexRegle].Appartenance1.nom);
                if( valTest > regleResultat[IndexRegle].Appartenance1.etat)
                    valTest = regleResultat[IndexRegle].Appartenance1.etat;

                if( valTest > value)
                    value = valTest;
            }


            //	Décommenter pour afficher touts les calcules
            //System.out.println("Intrval : "+i+"\tValeur : "+value);

            numerateur =  (i*value ) + numerateur;
            denominateur += value;

        }

        float resultat = numerateur/denominateur;
        System.out.print("CdG : ["+intervalMin+";"+intervalmax+"]");
        System.out.println("= "+ resultat);




    }

    private boolean between(float val1, float val2, float test) {
        if(test > val1 && test < val2){
            return true;
        }

        return false;
    }

    float max ( float val1, float val2) {
        return val1 > val2 ? val1 : val2;
    }

    float min ( float val1, float val2) {
        return val1 < val2 ? val1 : val2;
    }

    private int getIndexFromName(String nom) {

        switch (nom) {
            case "NM":
                return 0;
            case "NS":
                return 1;
            case "ZR":
                return 2;
            case "PS":
                return 3;
            case "PM":
                return 4;
            default:
                break;
        }

        return -1;
    }


    float FonctionAppartenance(float valeur, String str){

        switch (str) {
            case "NM":
                return max(min(1,(-45-valeur)/45),0);
            case "NS":
                return max(min((valeur+90)/45,(-valeur)/45),0);
            case "ZR":
                return max(min((valeur+45)/45,(45-valeur)/45),0);
            case "PS":
                return max(min(valeur/45,(-valeur+90)/45),0);
            case "PM":
                return max(min(1,(valeur-45)/45),0);
            default:
                break;
        }
        return -1;
    }



    private int Evaluation(float angle, Regle[] regle) {
        int nbRegle = 0;

        if(between(-90, -45, angle)) {
            regle[nbRegle].nom = "NM";
            regle[nbRegle].angle = angle;
            regle[nbRegle].min = -90;
            regle[nbRegle].max = -45;
            regle[nbRegle].etat = max(min(1,(-45-angle)/45),0);

            nbRegle++;
        }

        if(between(-90, 0, angle)) {
            regle[nbRegle].nom   = "NS";
            regle[nbRegle].angle = angle;
            regle[nbRegle].etat  = max(min((angle+90)/45,(-angle)/45),0);
            regle[nbRegle].min = -90;
            regle[nbRegle].max = 0;

            nbRegle++;
        }

        if(between(-45, 45, angle)) {
            regle[nbRegle].nom   = "ZR";
            regle[nbRegle].angle = angle;
            regle[nbRegle].etat  = max(min((angle+45)/45,(45-angle)/45)   ,0);
            regle[nbRegle].min = -45;
            regle[nbRegle].max = 45;

            nbRegle++;
        }

        if(between(0, 90, angle)) {
            regle[nbRegle].nom   = "PS";
            regle[nbRegle].angle = angle;
            regle[nbRegle].etat  = max(min(angle/45,(-angle+90)/45),0) ;
            regle[nbRegle].min = 0;
            regle[nbRegle].max = 90;

            nbRegle++;
        }

        if(between(45, 90, angle)) {
            regle[nbRegle].nom   = "PM";
            regle[nbRegle].angle = angle;
            regle[nbRegle].etat  = max(min(1,(angle-45)/45),0) ;
            regle[nbRegle].min = 45;
            regle[nbRegle].max = 90;

            nbRegle++;
        }

        return nbRegle;
    }
}
