
public class Regle {
    public String nom;
    public float angle;
    public float vitesse;
    public float max, min;
    public float etat;
    public int id;
    public Appartenance Appartenance1, Appartenance2;

    public Regle(String nom, float angle, float vitesse, float max, float min, float etat, int id, Appartenance Appartenance1, Appartenance Appartenance2) {
        this.nom = nom;
        this.angle = angle;
        this.vitesse = vitesse;
        this.max = max;
        this.min = min;
        this.etat = etat;
        this.id = id;
        this.Appartenance1 = Appartenance1;
        this.Appartenance2 = Appartenance2;
    }
}
